#!/bin/bash

mkdir public
mkdir public/component

curl -o Loupe.lock https://gitlab.gnome.org/GNOME/loupe/-/raw/HEAD/Cargo.lock
curl -o Snapshot.lock https://gitlab.gnome.org/GNOME/snapshot/-/raw/HEAD/Cargo.lock
curl -o Tour.lock https://gitlab.gnome.org/GNOME/gnome-tour/-/raw/HEAD/Cargo.lock
curl -o glycin.lock https://gitlab.gnome.org/GNOME/glycin/-/raw/HEAD/Cargo.lock
curl -o rsvg.lock https://gitlab.gnome.org/GNOME/librsvg/-/raw/HEAD/Cargo.lock

cargo r --release
