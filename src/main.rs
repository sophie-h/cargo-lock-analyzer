mod known_packages;

use std::fmt::Write;
use std::path::Path;
use std::time::Duration;

use cargo_lock::{dependency::Tree, Lockfile, Package};
use itertools::Itertools;

use petgraph::algo::simple_paths::all_simple_paths;

use known_packages::known_packages;

const PROJECTS: &[(&str, &str)] = &[
    ("Loupe", "Loupe.lock"),
    ("Snapshot", "Snapshot.lock"),
    ("Tour", "Tour.lock"),
    ("glycin", "glycin.lock"),
    ("rsvg", "rsvg.lock"),
];

fn main() {
    let mut packages = Packages::default();
    for (name, path) in PROJECTS {
        packages.load(name, path);
    }
    let outadated_duplicates = packages.outdated_duplicates();

    // index
    std::fs::write("public/index.html", index()).unwrap();

    // overview
    let content = list_all(&packages, &outadated_duplicates, Settings::default());
    std::fs::write("public/overview.html", content).unwrap();

    // per component pages
    for (name, _) in PROJECTS {
        let settings = Settings {
            hide_duplicate_versions: true,
            filter_project_name: Some(String::from(*name)),
        };

        let content = list_all(&packages, &outadated_duplicates, settings);

        std::fs::write(format!("public/component/{name}.html"), content).unwrap();
    }

    for package in known_packages().keys() {
        let package_used = packages
            .packages
            .iter()
            .any(|x| x.package.name.as_str() == *package);
        if !package_used {
            eprintln!("Ununsed package in known package list: {package}");
        }
    }
}

pub fn index() -> String {
    let mut s = String::new();

    s.push_str("<ul><li><a href='overview.html'>Overview</a>: List of all packages used in GNOME stack. Shows packages that are used with different version.</li></ul>");

    s.push_str("<ul>");
    for (project, _) in PROJECTS {
        s.push_str(&format!(
            "<li><a href='component/{project}.html'>{project}</a></li>"
        ));
    }
    s.push_str("</ul>");

    s
}

#[derive(Default)]
pub struct Settings {
    hide_duplicate_versions: bool,
    filter_project_name: Option<String>,
}

pub fn list_all(
    packages: &Packages,
    outdated_duplicates: &[&ProjectPackage],
    settings: Settings,
) -> String {
    let mut s = String::new();

    s.push_str(&std::fs::read_to_string("template.html").unwrap());

    if !settings.hide_duplicate_versions {
        s.push_str("<input type='checkbox' checked id='only-outdated' name='only-outdated'>");
        s.push_str("<label for='only-outdated'>Only show outdated duplicates</label>");
    }

    s.push_str("<table>");
    s.push_str(
        "<tr class=th><th>Project</th><th>Dependency</th><th>Version</th><th title='Number of GNOME packages that use this package'>Uses</th><th>Paths</th></tr>",
    );

    let mut package_count = 0;
    let mut package_count_known_vendor = 0;
    let mut package_count_rare_unknown = 0;

    for package in packages.packages.iter() {
        // Skip internal packages
        if package.package.source.is_none() {
            continue;
        }

        if settings.filter_project_name.is_some()
            && Some(&package.project.name) != settings.filter_project_name.as_ref()
        {
            continue;
        }

        let is_outdated_duplicate = outdated_duplicates
            .iter()
            .any(|x| semver_eq(&x.package, &package.package));

        let is_recent_duplicate = outdated_duplicates
            .iter()
            .any(|x| x.package.name == package.package.name);

        if settings.hide_duplicate_versions && is_outdated_duplicate {
            continue;
        }

        let duplicate = if is_outdated_duplicate {
            " data-duplicate"
        } else if is_recent_duplicate {
            " data-duplicate-recent"
        } else {
            ""
        };

        let path_out = if is_outdated_duplicate {
            let graph = package.project.tree.graph();
            let nodes = package.project.tree.nodes();
            let roots = package.project.tree.roots();

            let mut path_out = String::from("<details><summary>Show paths</summary>");

            for from in roots {
                let to = nodes
                    .iter()
                    .find(|(x, _)| x.matches(&package.package))
                    .unwrap()
                    .1;

                let paths = all_simple_paths::<Vec<_>, _>(graph, from, *to, 1, None);
                for path in paths {
                    let named_path: Vec<String> = path
                        .into_iter()
                        .map(|x| graph[x].name.to_string())
                        .collect();
                    write!(path_out, "<p>{}</p>", named_path.join("➞")).unwrap();
                }
            }

            write!(path_out, "</details>").unwrap();

            path_out
        } else {
            String::new()
        };

        let n_uses = package.n_uses(packages);

        let known_package = if let Some(vendor) = package.known_vendor() {
            package_count_known_vendor += 1;
            format!(" class='known-package' title='Known vendor: {vendor}'")
        } else {
            String::new()
        };

        let rare_unknown_package = if n_uses == 1 && package.known_vendor().is_none() {
            package_count_rare_unknown += 1;
            " class='rare-unused' title='Only used in this component and unknown vendor'"
        } else {
            ""
        };

        s.push_str(&format!(
            "<tr{duplicate}><td>{project}</td><td{known_package}><a href='https://crates.io/crates/{package}'>{package}</a></td><td>{version}</td><td{rare_unknown_package}>{n_uses}</td><td>{path}</td></tr>",
            project = package.project.name,
            package = package.package.name,
            version = package.package.version,
            n_uses = n_uses,
            path = path_out
        ));

        package_count += 1;
    }

    s.push_str("</table>");

    if settings.hide_duplicate_versions {
        s.push_str(&format!("<p>Total packages in table: {package_count} (from known vendors: {package_count_known_vendor})</p>"));
        s.push_str(&format!("<p>Packages only used in this project with unknown vendor: {package_count_rare_unknown}</p>"));
    }

    s
}

#[derive(Clone, Debug)]
pub struct Project {
    name: String,
    tree: Tree,
}

#[derive(Clone, Debug)]
pub struct ProjectPackage {
    project: Project,
    package: Package,
}

pub fn semver_eq(a: &Package, b: &Package) -> bool {
    if a.name != b.name || a.version.pre != b.version.pre {
        false
    } else if a.version.major == 0 && b.version.major == 0 {
        a.version.minor == b.version.minor
    } else {
        a.version.major == b.version.major
    }
}

impl ProjectPackage {
    pub fn new(project: Project, package: Package) -> Self {
        Self { project, package }
    }

    pub fn is_alternate_version(&self, b: &Self) -> bool {
        if self.package.name != b.package.name {
            false
        } else {
            !semver_eq(&self.package, &b.package)
        }
    }

    pub fn known_vendor(&self) -> Option<&&'static str> {
        known_packages().get(self.package.name.as_str())
    }

    pub fn n_uses(&self, packages: &Packages) -> usize {
        let package = self.package.name.as_str();
        packages
            .packages
            .iter()
            .filter(|x| x.package.name.as_str() == package)
            .count()
    }

    pub fn is_top_100(&self) -> bool {
        let file = std::fs::read_to_string("cache/top-100").unwrap();
        let top_100 = file.lines().collect::<Vec<_>>();

        let package = self.package.name.as_str();

        top_100.contains(&package)
    }

    pub fn source(&self) -> Option<String> {
        let package = self.package.name.as_str();
        let package_cache_path = format!("cache/{package}");

        if let Ok(source) = std::fs::read_to_string(&package_cache_path) {
            return Some(source);
        }

        let client = crates_io_api::SyncClient::new(
            "GNOME cargo-lock-tracker (sophieherold@gnome.org)",
            Duration::from_secs(1),
        )
        .unwrap();

        let cr = match client.get_crate(package) {
            Err(err) => {
                eprintln!("Failed to get {package}: {err}");
                return None;
            }
            Ok(data) => data,
        };

        let repo = cr.crate_data.repository;

        if let Some(repo) = &repo {
            std::fs::write(&package_cache_path, repo).unwrap();
        }

        repo
    }
}

#[derive(Clone, Debug, Default)]
pub struct Packages {
    packages: Vec<ProjectPackage>,
}

impl Packages {
    pub fn load(&mut self, project_name: &str, path: impl AsRef<Path>) {
        let lockfile = match Lockfile::load(path) {
            Ok(lockfile) => lockfile,
            Err(err) => {
                panic!("Failed to load .lock-file for {project_name}:\n {err}");
            }
        };

        let project = Project {
            name: project_name.to_string(),
            tree: lockfile.dependency_tree().unwrap(),
        };

        let mut packages = lockfile
            .packages
            .into_iter()
            .map(|x| ProjectPackage::new(project.clone(), x))
            .collect();

        self.packages.append(&mut packages);
        self.sort();
    }

    fn sort(&mut self) {
        self.packages.sort_by(|a, b| {
            a.package
                .name
                .cmp(&b.package.name)
                .then_with(|| a.package.version.cmp(&b.package.version))
        });
    }

    pub fn outdated_duplicates(&self) -> Vec<&ProjectPackage> {
        self.packages
            .iter()
            .circular_tuple_windows()
            .filter_map(|(package, next_package)| {
                if package.is_alternate_version(next_package) {
                    Some(package)
                } else {
                    None
                }
            })
            .collect()
    }
}
