use crates_io_api::{CratesQuery, Sort, SyncClient};
fn main() {
    let client = SyncClient::new(
        "GNOME script (sophieherold@gnome.org)",
        std::time::Duration::from_millis(1000),
    )
    .unwrap();

    let query = CratesQuery::builder()
        .sort(Sort::RecentDownloads)
        .page_size(100)
        .build();

    let crates = client.crates(query).unwrap();
    for c in crates.crates {
        println!("{}", c.id);
    }
}
